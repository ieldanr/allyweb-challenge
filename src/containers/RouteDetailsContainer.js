import {selectRouteAction, fetchRoutes} from '../actions/SelectRouteAction';
import { connect } from 'react-redux';
import RouteDetails from '../components/RouteDetails';

const mapStateToProps = (state) => {
  return {
    selectedRoute: state.selectedRoute
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    clickAndSelectRoute: (route) => {
      dispatch(selectRouteAction(route));
    }
  };
}

const RouteDetailsContainer = connect(
  mapStateToProps, mapDispatchToProps
)(RouteDetails);

export default RouteDetailsContainer;
