import {selectRouteAction, fetchRoutes} from '../actions/SelectRouteAction';
import { connect } from 'react-redux';
import RouteResults from '../components/RouteResults';

const mapStateToProps = (state) => {
  return {
    selectedRoute: state.selectedRoute,
    fetchedRoutes: state.fetchedRoutes
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    clickAndSelectRoute: (route) => {
      dispatch(selectRouteAction(route));
    },
    clickAndRequestRoutes: () => {
      dispatch(fetchRoutes());
    }
  }
}

const RouteResultsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RouteResults);

export default RouteResultsContainer;
