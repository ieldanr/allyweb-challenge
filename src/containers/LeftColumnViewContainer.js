import {selectRouteAction, fetchRoutes} from '../actions/SelectRouteAction';
import { connect } from 'react-redux';
import LeftColumnView from '../components/LeftColumnView';

const mapStateToProps = (state) => {
  return {
    selectedRoute: state.selectedRoute
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    clickAndRequestRoutes: () => {
      dispatch(fetchRoutes());
    }
  };
}

const LeftColumnViewContainer = connect(
  mapStateToProps, mapDispatchToProps
)(LeftColumnView);

export default LeftColumnViewContainer;
