import types from '../constants/ActionTypes';
import fetch from '../core/fetch';

export function selectRouteAction(route){
  return {
    type: types.SELECT_ROUTE,
    route: route
  }
}

export function requestRoutes(){
  return {
    type: types.REQUEST_ROUTES,
  };
}

export function fetchRoutesSuccess(json){
  return {
    type: types.FETCH_ROUTES_SUCCESS,
    allyRoutes: json.data.routes,
  }
}

export function fetchRoutes(){
  return dispatch => {
    dispatch(requestRoutes());
    return fetch('/graphql?query={routes{price{currency,amount},type,provider,segments{name,travel_mode,stops{lat,lng,datetime,name}}}}')
        .then(req => req.json())
        .then(json => {
          dispatch(fetchRoutesSuccess(json))
        })
  }
}
