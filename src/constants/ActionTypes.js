/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import keyMirror from 'fbjs/lib/keyMirror';

const mirror = keyMirror({
  'SELECT_ROUTE': null,
  'REQUEST_ROUTES': null,
  'FETCH_ROUTES_SUCCESS': null
});

export default mirror;
