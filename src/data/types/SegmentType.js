/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import {
  GraphQLObjectType as ObjectType,
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
  GraphQLInt as Int,
  GraphQLList as List,
} from 'graphql';
import StopType from './StopType'

const SegmentType = new ObjectType({
  name: 'Segment',
  fields: {
    name: { type: StringType },
    num_stops: { type: Int },
    travel_mode: { type: StringType },
    description: { type: StringType },
    stops: { type: new List(StopType)},
    color: { type: StringType },
    icon_url: { type: StringType }
  }
});

export default SegmentType;
