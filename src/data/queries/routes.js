/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import { GraphQLList as List } from 'graphql';
import fetch from '../../core/fetch';
import RouteType from '../types/RouteType';

const url = 'https://raw.githubusercontent.com/allyapp/web-code-challenge/' +
    'master/routes.json';

let items = [];

const routes = {
  type: new List(RouteType),
  resolve() {
    if (items.length == 0) {
      return fetch(url)
        .then(response => {
          return response.json();
        })
        .then(data => {
          items = data.routes;
          return items
        });
    }
    return items;
  },
};

export default routes;
