/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RouteSegment.scss';
import { ReactFitText } from 'react-fittext'

class RouteSegment extends Component {
  render() {
    var material_icons = {
      'walking': 'directions_walk',
      'subway': 'directions_subway',
      'bus': 'directions_bus',
      'driving': 'directions_car',
      'cycling': 'directions_bike',
    }

    var colors = {
      'vbb': '#9b59b6',
      'drivenow':'#2980b9',
      'car2go':'#16a085',
      'google':'#2c3e50',
      'nextbike':'#e74c3c',
      'callabike':'#2980b9',
      'taxi':'#f1c40f',
      'default': '#bdc3c7',
    }

    var segment = this.props.segment;
    var segment_icon = material_icons[segment.travel_mode];
    var segment_color = segment.travel_mode == 'walking' ? colors['default'] : colors[this.props.provider];
    if (segment.name == "Taxi"){
      segment_color = colors['taxi'];
      segment_icon = 'local_taxi';
    }
    var segment_title = segment.name || this.props.provider;
    var segment_title = segment.travel_mode == 'walking' ? "" : segment_title;

    var font_size = segment_title.length >= 6 ? 12 : 16;
    if(segment_icon){
      return (<div className={s.root + " small-2 columns"} style={{"backgroundColor": segment_color }}>
        <div className="material-icons">{ segment_icon }</div>
        <div style={{"fontSize": font_size}}>{segment_title}</div>
        </div>);
    } else {
      return null;
    }
  }
}

export default withStyles(RouteSegment, s);
