import React, {Component} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Search.scss';

class Search extends Component {
  render(){
    var onClick = () => { this.props.clickAndRequestRoutes() };
    return (<div className={s.root + " row"}>
              <img className={s.logo} src="intro-logo.svg"/>
              <input type="text" placeholder="origin"/>
              <input type="text" placeholder="destination"/>
              <a className={s.button + " button secondary"} onClick={onClick}>Search</a>
    </div>);
  }
}

export default withStyles(Search, s);
