/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RouteDetails.scss';
import _ from 'underscore';
import moment from 'moment';
import RouteResume from '../RouteResume/RouteResume'
import RouteDetailsSegment from '../RouteDetailsSegment/RouteDetailsSegment'
import RouteDetailsSegmentLast from '../RouteDetailsSegmentLast/RouteDetailsSegmentLast'

class RouteDetails extends Component {
  constructor(props){
    super(props);
    this.handleBackClick = this.handleBackClick.bind(this);
  }

  handleBackClick(){
    this.props.clickAndSelectRoute({})
  }

  render() {
    var selectedRoute = this.props.selectedRoute;
    if(_.isEmpty(selectedRoute) || selectedRoute == null){
      return null
    }
    else {
      var segments = selectedRoute.segments.map((segment, index) => {
        return <RouteDetailsSegment key={"segment"+index} provider={selectedRoute.provider} segment={segment}/>
      });
      var last_segment = _.last(selectedRoute.segments);
      segments.push(<RouteDetailsSegmentLast provider={selectedRoute.provider} segment={last_segment}/>);
      var returnRouteSelection = (<div style={{"cursor":"pointer"}} className="row material-icons" onClick={this.handleBackClick.bind(this)}>keyboard_arrow_left</div>)
      var showRouteResume = (<RouteResume allyroute={selectedRoute}/>)
      return (<div className={s.root}>{returnRouteSelection}{showRouteResume}{ segments }</div>);
    }
  }
}

export default withStyles(RouteDetails, s);
