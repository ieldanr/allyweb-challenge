import React, {Component, PropTypes} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './LeftColumnView.scss';
import RouteDetailsContainer from '../../containers/RouteDetailsContainer'
import RouteResultsContainer from '../../containers/RouteResultsContainer'
import Search from '../Search/Search';
import _ from 'underscore'

class LeftColumnView extends Component {
  render() {
    var selectedRoute = this.props.selectedRoute;
    var results = null;
    if(_.isEmpty(selectedRoute) || selectedRoute == null){
      results = (<RouteResultsContainer/>);
    } else {
      results =  (<RouteDetailsContainer/>);
    }
    return(<div className={s.root + " small-12 large-4 column"}>
      <Search clickAndRequestRoutes={this.props.clickAndRequestRoutes}/>
      <div className={s.results}>
      {results}
      </div>
    </div>)
  }
}

export default withStyles(LeftColumnView, s);
