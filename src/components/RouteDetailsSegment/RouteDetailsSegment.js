import React, {Component} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RouteDetailsSegment.scss'
import _ from 'underscore';
import moment from 'moment';

class RouteDetailsSegment extends Component {
  render(){
    var colors = {
      'vbb': '#9b59b6',
      'drivenow':'#2980b9',
      'car2go':'#16a085',
      'google':'#2c3e50',
      'nextbike':'#e74c3c',
      'callabike':'#2980b9',
      'taxi':'#f1c40f',
      'default': '#bdc3c7',
    }
    var provider = this.props.provider;
    var segment = this.props.segment;
    var segment_color = segment.travel_mode == 'walking' ||
        segment.travel_mode == 'change' ? colors['default'] : colors[provider];
    if (segment.name == "Taxi"){
      segment_color = colors['taxi'];
    }
    var first_stop = _.first(segment.stops);
    var last_stop = _.last(segment.stops);
    var travel_mode = segment.travel_mode;
    var first_date = moment(first_stop.datetime);
    var last_date = moment(last_stop.datetime);
    var time_to_do = last_date.from(first_date,
      true);
    var start_time_div = (<div className={s.start_time_div + " small-2 columns"}>{first_date.format("hh:mm")}</div>)
    var segment_color_div = (<div className={s.route_color_parent + " small-1 column"}>
        <div className={s.route_color_circle} style={{"backgroundColor": segment_color}}></div>
        <div className={s.route_color} style={{"backgroundColor": segment_color}}>
        </div>
      </div>)
    var name_div = (<div>{first_stop.name}</div>)
    var time_div = (<div>{time_to_do + " " + travel_mode}</div>)
    return(<div className={s.segment + " row"}>{start_time_div}{segment_color_div}<div className="small-9 columns">{name_div}{time_div}</div></div>)
  }
}

export default withStyles(RouteDetailsSegment, s);
