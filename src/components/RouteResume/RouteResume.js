import React, {Component} from 'react'
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RouteResume.scss';
import RouteSegment from '../RouteSegment/RouteSegment'
import _ from 'underscore';
import moment from 'moment';

class RouteResume extends Component {
  render(){
    var allyroute = this.props.allyroute;
    var segmentsComponents = allyroute.segments.map((segment, index) =>{
      return (<RouteSegment key={"segment"+index} segment={segment} provider={allyroute.provider}></RouteSegment>);
    });
    var routeTitle = allyroute.type == "public_transport" ? "Public Transportation" : allyroute.provider;
    routeTitle = routeTitle == "google" ? allyroute.type : routeTitle;
    routeTitle = routeTitle == "private_bike" ? "Private Bike" : routeTitle;

    // calculate time
    var firstSegment = _.first(allyroute.segments);
    var lastSegment = _.last(allyroute.segments);
    var firstStop = _.first(firstSegment.stops);
    var lastStop = _.last(lastSegment.stops);
    var firstDate = moment(firstStop.datetime);
    var lastDate = moment(lastStop.datetime);
    var routeTime = lastDate.from(firstDate, true);
    var startTime = firstDate.format("hh:mm");
    var endTime = lastDate.format("hh:mm");

    // get fare cost
    var currencyHash = {
      "EUR": "€"
    }
    var cost = "";
    if(allyroute.price != null){
      cost = currencyHash[allyroute.price.currency] + allyroute.price.amount/100;
    }

    return (<div onClick={this.props.onClick} className={s.root + " row"}>
              <div className={s.route_title + " row"}>
                <div className="float-left">{routeTitle}</div>
                <div className="float-right">{startTime + " -> " + endTime}</div>
              </div>
              <div className={s.route_subtitle + " row"}>
                <div className={s.route_time + " column small-3"}>{routeTime}</div>
                <div className="column small-9">{cost}</div>
              </div>
              { segmentsComponents }
              <hr/>
          </div>);
  }
}

export default withStyles(RouteResume, s);
