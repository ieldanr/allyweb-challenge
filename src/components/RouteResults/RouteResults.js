/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { Component, PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RouteResults.scss';
import RouteResume from '../RouteResume/RouteResume'

class RouteResults extends Component {
  constructor(props){
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleRouteClick = this.handleRouteClick.bind(this);
  }

  handleSearch(){
    this.props.clickAndRequestRoutes();
  }

  handleRouteClick(allyroute){
    this.props.clickAndSelectRoute(allyroute);
  }

  render() {
    var allyRoutes = this.props.fetchedRoutes.allyRoutes || [];
    var routes = allyRoutes.map(
      (allyroute, index) => {
      return (<div key={"allyroute"+index} className={s.result}><RouteResume allyroute={allyroute} onClick={() => this.handleRouteClick(allyroute)}/></div>)
    });
    return (<div className={s.root}>
            { routes }
          </div>)
  }
}

export default withStyles(RouteResults, s);
