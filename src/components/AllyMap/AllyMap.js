import React, {Component} from 'react';
import execEnvironment from 'fbjs/lib/ExecutionEnvironment';


class AllyMap extends Component {
  state = {
    scriptjsLoaderEnabled: false
  };

  componentDidMount(){
    this.setState({scriptjsLoaderEnabled: true});
  }
  render(){
    var map = null

    if(this.state.scriptjsLoaderEnabled) {
      const position = [52.520645, 13.40];
      var {Map, Marker, Popup, TileLayer} = require('react-leaflet');
      map = (
        <Map style={{"height": "100vh"}} center={position} zoom={13}>
          <TileLayer
            id="ieldanr.pcjop818"
            accessToken="pk.eyJ1IjoiaWVsZGFuciIsImEiOiJjaWxucGx1aTYwMDFtdWlsdTdremxxNjIxIn0.H1XZD836C4dwbhnI0j9QjA"
            url='https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}'
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          />
          <Marker position={position}>
            <Popup>
              <span>A pretty CSS3 popup.<br/>Easily customizable.</span>
            </Popup>
          </Marker>
        </Map>
      );
    }
    return map;
  }
}

export default AllyMap;
