/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AllyRoutes.scss';
import LeftColumnViewContainer from '../../containers/LeftColumnViewContainer';
import AllyMap from '../../components/AllyMap/AllyMap';

function AllyRoutes({ allyRoutes }) {
  return (
    <div className={s.root}>
      <div className="row expanded">
          <LeftColumnViewContainer/>
          <AllyMap/>
      </div>
    </div>
  );
}

AllyRoutes.propTypes = {
  allyRoutes: PropTypes.arrayOf(PropTypes.shape({
    provider: PropTypes.string.isRequired,
    segments: PropTypes.arrayOf(PropTypes.shape({
      travel_mode: PropTypes.string.isRequired,
    })).isRequired,
  })).isRequired,
};

export default withStyles(AllyRoutes, s);
