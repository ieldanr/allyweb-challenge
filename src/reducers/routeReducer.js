import types from '../constants/ActionTypes';
import { combineReducers } from 'redux';

function selectedRoute(state = {}, action){
  switch(action.type) {
    case types.SELECT_ROUTE:
      return Object.assign({}, action.route);
    default:
      return state;
  }
}

function fetchedRoutes(state = { isFetching: false, allyRoutes: [] }, action){
  switch(action.type) {
    case types.REQUEST_ROUTES:
      return Object.assign({}, state, { isFetching: true });
    case types.FETCH_ROUTES_SUCCESS:
      return Object.assign({}, state, { isFetching: false, allyRoutes: action.allyRoutes });
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  selectedRoute,
  fetchedRoutes
});

export default rootReducer;
